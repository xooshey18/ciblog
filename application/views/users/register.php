<?php echo form_open('users/register'); ?>
<div class="row">
	<div class="col-md-4 col-md-offset-4">
		<h2 class="text-center"><?= $title; ?></h2>
		<center><?php echo validation_errors(); ?></center>
			<div class="form-group">
				<label>Name</label>
				<input type="text" name="name" title="Name" placeholder="Enter Name" class="form-control">	
			</div>
			<div class="form-group">
				<label>Zipcode</label>
				<input type="text" name="zipcode" title="Zipcode" placeholder="Zipcode" class="form-control">	
			</div>
			<div class="form-group">
				<label>Email</label>
				<input type="text" name="email" title="Email" placeholder="Enter Email" class="form-control">	
			</div>
			<div class="form-group">
				<label>User Name</label>
				<input type="text" name="username" title="Username" placeholder="Enter Username" class="form-control">	
			</div>
			<div class="form-group">
				<label>Password</label>
				<input type="password" title="Password" name="password" placeholder="Enter Password" class="form-control">	
			</div>
			<div class="form-group">
				<label>Confirm Password</label>
				<input type="password" title="Confirm Password" name="password2" placeholder="Confirm Password" class="form-control">	
			</div>
			<button type="submit" class="btn btn-primary btn-block">Register</button>
		</div>
</div>
<?php echo form_close(); ?>

